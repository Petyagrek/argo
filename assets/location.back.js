/* Behaviour for Locations Page
   http://argotea-2.myshopify.com/pages/locations */



_AT = _AT || {};

var argoGlobals = {
  map: null,
  markers: [],          // Init empty array so that markers can be pushed to it.
  iws: [],              // For all InfoWindows.
  openInfoWindow: null  // Instance of InfoWindow which is in open state.
};

var locationsDebugData;


jQuery(function($) {
  _AT.locations();
});


_AT.locations = function() {
  if(!$('body').hasClass('page-locations')) return;
  var map;
  var bounds;
  var service;


  function initialize() {
    var mapOptions = {
      center: { lat: 41.872458, lng: -87.676907 }, // Center at Chicago. Overriden later.
      zoom: 8,
      navigationControl: false,
      mapTypeControl: false,
      scaleControl: false,
      // draggable: false,
      scrollwheel: false
    };
    map = new google.maps.Map(document.getElementById('locationsMap'), mapOptions);
    argoGlobals.map = map;

    bounds = new google.maps.LatLngBounds();

  }
  google.maps.event.addDomListener(window, 'load', initialize);



  argoGlobals.locationIds = [
    'ChIJETsFTaUsDogRwB5N0aWJ-kE', // State & Randolph ✔
    'ChIJP_NOo7wsDogR9TcBHPMn2GM', // Dearborn & Adams ✔
    'ChIJ6dpUbjkpDogRSuTvQmsXywA', // 5758 S Maryland Ave ✔
    'ChIJ43ZVwabTD4gRfnQpDg-taIo', // 3135 North Broadway ✔
    'ChIJ-72Y31TTD4gRk8E39cYxIRA', // Northwestern Memorial Hospital ✔
    'ChIJV0LZUbYsDogRU8BFdH1OjPM', // 222 W Merchandise Mart Plaza ✔
    'ChIJpafdA1PTD4gRTbdqGGo3j7I', // 819 N Rush St, Chicago, IL, United States ✔
    'ChIJqQyWfSa0D4gRN_IZ45o_HCE', // Terminal 2 ✔
    'ChIJRSn-ONe1D4gR9yZD1uq6t4k', // Terminal 3, O'Hare International Airport, Chicago, IL, United States ✔
    'ChIJ6QM3Tim0D4gROoQMax1M5oE', // Terminal 3
    'ChIJA98_PLksDogRa_L2mJG8kAA', // Franklin & Madison ✔
    'ChIJu_tp4r4sDogRLarHLYUcYUs', // The Willis Tower - Wacker Drive ✔
    'ChIJ-QQV-aNZwokRfC_bYyZuq6E', // 949 Broadway ✔
    'ChIJcb-IBQ9ZwokRrMAlKCsa4_U', // Nyu Medical Center: Bakshy Aric MD ✔
    'ChIJ8-NotfdYwokRnXBumM5iYW8', // Columbus Circle ✔
    'ChIJRygdwplZwokRhLLHDf10kCA', // Union Square ✔
    'ChIJj2q4SKVZwokR4ifdE5XHjyQ', // Chelsea ✔
    'ChIJb6CL_qssDogRCC9pTVDZWBk', // Tribune Tower ✔
    'ChIJ2d67RVLTD4gRtkRmcRqUB_M', // Connors Park ✔
    'ChIJS_rmxJavD4gRw6AAriTq_AI', // Woodfield Mall ✔
    'ChIJu7jHY5BZwokRMXOlqm74HQM', // 239 Greene St ✔
    'ChIJ_zScxzxmXj4R3qCFAp08HmE', // Al Wahdah ✔
    'ChIJASzND9cS04kRjIJ-qAVQvg8', // Buffalo State ✔
    'ChIJWSc92xgKzz8RM1NpafGVQe0', // The Gate Mall ✔
    'ChIJVWuaL2Sczz8RbwhGhHVXAMo', // Tavern 2 ✔
    'ChIJy0lRgWhCXz4R--6Y4fKOP84' // Al Safa Road, City Walk - Dubai ✔
  ];
  var locationIds = argoGlobals.locationIds;



  var processLocation = function(place, count) {

    var address= '';
    if ('formatted_address' in place) {
      address = place.formatted_address;
    }

    var marker = new google.maps.Marker({
      map: argoGlobals.map,
      position: place.geometry.location
    });
    argoGlobals.markers.push({a: address, m: marker});

    var infowindow = new google.maps.InfoWindow();
    var content;
    google.maps.event.addListener(marker, 'click', function() {
      content = '<h3>' + place.name + '</h3>';
      if ('opening_hours' in place) {
        var hours;
        content += '<pre>';
        for (var c = 0; c < place.opening_hours.weekday_text.length; c++) {
          hours = place.opening_hours.weekday_text[c];
          hours = hours.replace('Sunday:', 'Sunday:   ');
          hours = hours.replace('Monday:', 'Monday:   ');
          hours = hours.replace('Tuesday:', 'Tuesday:  ');
          hours = hours.replace('Wednesday:', 'Wednesday:');
          hours = hours.replace('Thursday:', 'Thursday :');
          hours = hours.replace('Friday:', 'Friday:   ');
          hours = hours.replace('Saturday:', 'Saturday: ');
          content += hours + '<br />';
        }
        content += '</pre>';
      }
      content += '<p>' + place.adr_address + '</p>';
      if ('international_phone_number' in place) content += '<em class="pull-left">' + place.international_phone_number + '</em>';
      content += '<div class="pull-right"><a href="http://maps.google.com/maps?saddr=&daddr=' + marker.getPosition() + '" target="_blank" class="btn btn-xs btn-purple">GET DIRECTIONS</a></div>'
      infowindow.setContent(content);
      infowindow.open(argoGlobals.map, this);
    });
    argoGlobals.iws.push(infowindow);

    // bounds.extend(place.geometry.location);
    // argoGlobals.map.fitBounds(bounds);  /* !!! Place it outside this function, and outside the loop which calls this function for better performance. */

    if (count === locationIds.length - 1) { $('.locations-cont .loading').slideUp(); argoGlobals.showNearestStore2 = showNearestStore2(); }
  };

  setTimeout(function() {
    service = new google.maps.places.PlacesService(argoGlobals.map);
    for (var count = 0; count < locationIds.length; count++) {
      setTimeout(function(count) {
        var request = { placeId: locationIds[count] };
        
        service.getDetails(request, function (place, status) {
          if (status == google.maps.places.PlacesServiceStatus.OK) {
            
            processLocation(place, count);
          } else {
            console.error(count, locationIds[count], status, place);
          }
        });
      }.bind(undefined, count), count * 400);
    }  // end for
  }, 3000);


  var showNearestStores = function() {
    // Get user's current location
    if ("geolocation" in navigator) {
      /* geolocation is available */
      navigator.geolocation.getCurrentPosition(function(position) {
        
        setTimeout(function() {
          argoGlobals.map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
          argoGlobals.map.setZoom(6);
        }, 500);
      });
    } else {
      /* geolocation IS NOT available */
      $('.show-nearest-stores').slideUp();
      alert("Couldn't find location.");
    }
  };
  // showNearestStores();


  var searchLocations = function() {
    $('#search-locations-input').focus(function() {
      $( '#search-locations-input' ).scrollHere(parseInt($( '#top-nav-new' ).css('height')));
    });


var hideAllMarkers = function(all) {
  var bounds = new google.maps.LatLngBounds();
    for (var count = 0; count < all.length; count++) {
      all[count].m.setVisible(false);
      bounds.extend(all[count].m.getPosition());
    }
    setTimeout(function() {
      argoGlobals.map.fitBounds(bounds);
    }, 500);
};

var showAllMarkers = function(all) {
  var bounds = new google.maps.LatLngBounds();
    for (var count = 0; count < all.length; count++) {
      all[count].m.setVisible(true);
      bounds.extend(all[count].m.getPosition());
    }
    setTimeout(function() {
      argoGlobals.map.fitBounds(bounds);
    }, 500);
};

$('#search-locations-input').keyup(function() {
  var searchTerm = $(this).val().toLowerCase(),
    bounds = new google.maps.LatLngBounds(),
    all = argoGlobals.markers,
    bounds = new google.maps.LatLngBounds();
  if (searchTerm.length > 0) {
    hideAllMarkers(all);
    // Run search only if at least 1 character is entered in search input.
    for (var count = 0; count < all.length; count++) {
      var re = new RegExp('(' + searchTerm + ')', 'i');
      var address = all[count].a;
      var marker = all[count].m;

      if (address.match(re) !== null) {
        marker.setVisible(true);
        bounds.extend(marker.getPosition());
        $('#search-locations-input').tooltip('hide');
      } else {
        // Hide marker
        showAllMarkers(all);
        $('#search-locations-input').tooltip('show');
      }
    }  // End for
    setTimeout(function() {
      argoGlobals.map.fitBounds(bounds);
      if (argoGlobals.map.zoom > 15) argoGlobals.map.setZoom(15);
    }, 500);
  }
  else {
    // If search input is cleared, show all locations.
    var bounds = new google.maps.LatLngBounds();
    for (var count = 0; count < all.length; count++) {
      all[count].m.setVisible(true);
      bounds.extend(all[count].m.getPosition());
    }
    setTimeout(function() {
      argoGlobals.map.fitBounds(bounds);
    }, 500);
  }
});

  };  // End searchLocations()
  searchLocations();


  //$('.show-nearest-stores').click(showNearestStores);
  $('.show-nearest-stores').click(showNearestStore2);



  var showNearestStore2 = function() {
    
    // Show nearest store
    // Zoom map to show nearest store

    // print all distances
    var cl,  // Current Location
      dl,    // Destination Location
      service = new google.maps.DistanceMatrixService(),
      minDistance = {},
      lastLatLng,
      pairs = [];
    minDistance.distance = Infinity;

    var showNearest = function() {
     
      // var nearest = Infinity;
      // for (var count = 0; count < minDistance.length; count++) {
      //   if (minDistance[count].distance < nearest) nearest = minDistance[count];
      // }
      
      argoGlobals.map.setCenter(minDistance.pos);
      argoGlobals.map.setZoom(15);
    }
    var distanceCount = 0;

    function callback(response, status) {
      // See Parsing the Results for
      // the basics of a callback function.
      
      if (status == google.maps.DistanceMatrixStatus.OK) {
        var origins = response.originAddresses;
        var destinations = response.destinationAddresses;

        

        for (var i = 0; i < origins.length; i++) {
          var results = response.rows[i].elements;
          for (var j = 0; j < results.length; j++) {
            var element = results[j];
            console.info(element.distance !== undefined, element, typeof element);
            if (element.distance !== undefined) {
              
              pairs.push({ll: destinations, d: element.distance.value});
           
              if (element.distance.value < minDistance.distance) {
                console.info(element.distance.value); 
                // minDistance = element.distance.value;
                // minDistance = {distance: element.distance.value, pos: destinations[0]};
                minDistance = {distance: element.distance.value, pos: argoGlobals.markers[distanceCount].m.position};
              }
            }
            //var distance = element.distance.text;
          }
        }
        distanceCount++;
        if (distanceCount === argoGlobals.markers.length) showNearest();
      }
    }

    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(function(position) {
        
        cl = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        cl = new google.maps.LatLng(41.928055, -87.769234); // Chicago for testing

        for(var count = 0; count < argoGlobals.markers.length; count++) {
          dl = argoGlobals.markers[count].m.position;

          service.getDistanceMatrix({
            origins: [cl],
            destinations: [dl],
            travelMode: google.maps.TravelMode.DRIVING
          }, callback);

          if(count === argoGlobals.markers.length - 1) {
            lastLatLng = dl;
           
          }
        }

      });
    }
    else {
      /* geolocation IS NOT available */
      $('.show-nearest-stores').slideUp();
      alert("Couldn't find location.");
    }

    return {
      cl: cl,
      dl: dl,
      lastLatLng: lastLatLng,
      pairs: pairs
    }

  };  // End showNearestStore2()


};
