/* Behaviour for Locations Page
   http://argotea-2.myshopify.com/pages/locations */



var _AT = _AT || {};

var argoGlobals = {
  map: null,
  markers: [],          // Init empty array so that markers can be pushed to it.
  iws: [],              // For all InfoWindows.
  openInfoWindow: null,  // Instance of InfoWindow which is in open state.
  autocomplete: {},
  doneReq: false,
  userLocation: {},
  storePlace: {}
};

var locationsDebugData;


jQuery(function($) {
  _AT.locations();
});


_AT.locations = function() {
  if(!$('body').hasClass('page-locations')) return;

  var map;
  var bounds;
  var service;


  function initialize() {
    var mapOptions = {
      center: { lat: 41.872458, lng: -87.676907 }, // Center at Chicago. Overriden later.
      zoom: 12,
      navigationControl: false,
      mapTypeControl: false,
      scaleControl: false,
      // draggable: false,
      scrollwheel: false
    };
    geocoder = new google.maps.Geocoder();
    map = new google.maps.Map(document.getElementById('locationsMap'), mapOptions);
    argoGlobals.map = map;
    bounds = new google.maps.LatLngBounds();
  Init2();
    var input = document.getElementById('search-locations-input');
    var autocomplete = new google.maps.places.Autocomplete(input);
    argoGlobals.autocomplete = autocomplete;
  }
  google.maps.event.addDomListener(window, 'load', initialize);



  // Place IDs can be found at:
  // https://developers.google.com/maps/documentation/javascript/examples/places-placeid-finder
  argoGlobals.locationIds = [
    'ChIJETsFTaUsDogRwB5N0aWJ-kE', // 16 W Randolph St, Chicago ✔
    'ChIJP_NOo7wsDogR9TcBHPMn2GM', // Dearborn & Adams ✔
    'ChIJ6dpUbjkpDogRSuTvQmsXywA', // 5758 S Maryland Ave ✔
    'ChIJ43ZVwabTD4gRfnQpDg-taIo', // 3135 North Broadway ✔
    'ChIJ-72Y31TTD4gRk8E39cYxIRA', // Northwestern Memorial Hospital ✔
    'ChIJV0LZUbYsDogRU8BFdH1OjPM', // 222 W Merchandise Mart Plaza ✔
    'ChIJpafdA1PTD4gRTbdqGGo3j7I', // 819 N Rush St, Chicago, IL, United States ✔
    'ChIJqQyWfSa0D4gRN_IZ45o_HCE', // Terminal 2 ✔
    'ChIJRSn-ONe1D4gR9yZD1uq6t4k', // Terminal 3, O'Hare International Airport, Chicago, IL, United States ✔
    'ChIJ6QM3Tim0D4gROoQMax1M5oE', // Terminal 3 ✔
    'ChIJA98_PLksDogRa_L2mJG8kAA', // Franklin & Madison ✔
    'ChIJu_tp4r4sDogRLarHLYUcYUs', // The Willis Tower - Wacker Drive ✔
    'ChIJ-QQV-aNZwokRfC_bYyZuq6E', // 949 Broadway ✔
    'ChIJcb-IBQ9ZwokRrMAlKCsa4_U', // Nyuu Medical Center: Bakshy Aric MD ✔
    'ChIJ8-NotfdYwokRnXBumM5iYW8', // 1792 Broadway, New York ✔
    'ChIJj2q4SKVZwokR4ifdE5XHjyQ', // 275 7th Ave, New York ✔
    'ChIJb6CL_qssDogRCC9pTVDZWBk', // Tribune Tower ✔
    'ChIJ2d67RVLTD4gRtkRmcRqUB_M', // Connors Park ✔
    'ChIJS_rmxJavD4gRw6AAriTq_AI', // Woodfield Mall ✔
    'ChIJu7jHY5BZwokRMXOlqm74HQM', // 239 Greene St ✔
    'ChIJ_zScxzxmXj4R3qCFAp08HmE', // Al Wahdah ✔
    'ChIJASzND9cS04kRjIJ-qAVQvg8', // Buffalo State ✔
    'ChIJy0lRgWhCXz4R--6Y4fKOP84', // Al Safa Road, City Walk - Dubai ✔
    
    'ChIJ4VzPmRl644kRjLkiZeYAe8k', // Northeastern University
    'ChIJ-yOxdUNMDogRkc2bvyRNdzw', // Oakbrook Mall
    'ChIJpSXH7OgsDogRrHNvMyu42js', // University of Illinois at Chicago
    'ChIJjzmdDB-jGYgR-jLs4LNnA-o', // Grand Valley State University
    'ChIJAVkD4S_99IgRSLCSGO-jF0E', // Atlanta International Airport 
    'ChIJ3cRJt0iGs4kRRliLzMIIUUA', // University Of Virginia
    'ChIJ65GMS-Ah9ocRMbFEZ5Um2eo', // UnitedHealth Group, Minnesota
    'ChIJ8QxqrKw1R4YRGtaELHbGOpc', // ExxonMobil
    'ChIJRxq6yTad44kRqpAmDsj5H4Y', // Bentley University, Waltham
    'ChIJNcvFFTsL3okRwjfsDm0SUVE', // University of Albany
    'ChIJ3RFkxAkpU4gRCZzO6Zydjk4', // Elon University
    'ChIJs9U8YuPJt4kRzrhvQqTY8i4', // American University
    'ChIJdTmri3qJsIkRQN3gvl6vw70', // College of William and Mary
    'ChIJC-tmgWhCXz4RL3vgur4gUkc', // City Walk, Dubai
    'ChIJq5JzPW_cRT4RtqbA-pWTHQI', // Gulf Mall, Doha
    'ChIJpdZYwbDIRT4R0zDtL-03UYU', // Hamad International Airport
    'ChIJDwOafn1FXj4RGAdtSOLgiek', // Yas Mall
    'ChIJ3XjItzxmXj4RUircWbG4pgE', // Al Wahda Mall Hazza Bin Zayed St
    'ChIJHfeKyWTcRT4R_Jc167s7Xq0'  // Landmark Mall
    
    
    
    //'ChIJjdor14Tmt4kRdwPWxeHN7G4', // Fort Meade, MD
    //'ChIJWSc92xgKzz8RM1NpafGVQe0', // The Gate Mall ✔
    //'ChIJRygdwplZwokRhLLHDf10kCA', // Union Square ✔
    //'ChIJvRAbEIwELz4R2U1lO8VrtVQ', // Riyadh Avenue Mall
    //'ChIJc62H4EuLWYgRuh1Xif87e8Y', // UNC
    //'ChIJVWuaL2Sczz8RbwhGhHVXAMo', // Tavern 2 ✔
  ];
  
  var locationIds = argoGlobals.locationIds;



  var getFormattedHours = function(place) {
    if ('opening_hours' in place) {
      var hours = '';
      var content = '<pre>';
      for (var c = 0; c < place.opening_hours.weekday_text.length; c++) {
        hours = place.opening_hours.weekday_text[c];
        hours = hours.replace('Sunday:', 'Sunday:   ');
        hours = hours.replace('Monday:', 'Monday:   ');
        hours = hours.replace('Tuesday:', 'Tuesday:  ');
        hours = hours.replace('Wednesday:', 'Wednesday:');
        hours = hours.replace('Thursday:', 'Thursday :');
        hours = hours.replace('Friday:', 'Friday:   ');
        hours = hours.replace('Saturday:', 'Saturday: ');
        content += hours + '<br />';
      }
      content += '</pre>';
    }
    return content;
  }


  var processLocation = function(place, count, stat) {
    if(stat){
      var address= '';
      if ('formatted_address' in place) {
        address = place.formatted_address;
      }

      /*try {
        
     
      }
      catch(e) {
        console.warn(e);
      }*/

      var marker = new google.maps.Marker({
        map: argoGlobals.map,
        position: place.geometry.location,
        icon: 'https://cdn.shopify.com/s/files/1/0909/1186/t/6/assets/marker.png?l'
      });
      argoGlobals.markers.push({a: address, m: marker});

      // var infowindow = new google.maps.InfoWindow();
      var content;
      google.maps.event.addListener(marker, 'click', function() {
        for (c = 0; c < argoGlobals.iws.length; c++) {
          // Close other infowindows which are open.
          argoGlobals.iws[c].close();
        }
        var infowindow = new google.maps.InfoWindow();
        content = '<h3>' + place.name;
        content += '<div class="pull-right"><a href="http://maps.google.com/maps?saddr=' + argoGlobals.userLocation + '&daddr=' + marker.getPosition() + '" target="_blank" class="btn btn-xs btn-purple">GET DIRECTIONS</a></div>';
        content += '</h3>';
        content += getFormattedHours(place);
        content += '<p>' + place.adr_address + '</p>';
        if ('international_phone_number' in place) content += '<em class="pull-left">' + place.international_phone_number + '</em>';
        infowindow.setContent(content);
        infowindow.open(argoGlobals.map, this);
        argoGlobals.iws.push(infowindow);
      });

      // bounds.extend(place.geometry.location);
      // argoGlobals.map.fitBounds(bounds);  /* !!! Place it outside this function, and outside the loop which calls this function for better performance. */
    }
    else {
      argoGlobals.markers.push({a: null, m: null});
    }

  };

  function populateLocalStorage(key, value) {
    if (localStorage.getItem(key) == null) {
      localStorage.setItem(key, value);
    }
  }

  var Init2 = function() {
    var stat = true;
    service = new google.maps.places.PlacesService(argoGlobals.map);
    for (var count = 0; count < locationIds.length; count++) {
      setTimeout(function(count) {
        var request = { placeId: locationIds[count] };
        
        service.getDetails(request, function (place, status) {
          if (status == google.maps.places.PlacesServiceStatus.OK) {
            if ('localStorage' in window) {
              populateLocalStorage(locationIds[count], place.geometry.location.lat() + ',' + place.geometry.location.lng());
            }
           
            argoGlobals.storePlace[count] = place;
            processLocation(place, count, stat);
          } else {
            argoGlobals.failReq = argoGlobals.failReq + 1;
            stat = false;
            argoGlobals.storePlace[count] = null;
            console.error(count, locationIds[count], status, place);
            processLocation(place, count, stat);
          }
         
          if(count == locationIds.length -1){
            argoGlobals.doneReq = true;
            if (argoGlobals.doneReq) {
              // $('.locations-cont .loading').slideUp();
              // After all markers are added, show nearest stores.
              argoGlobals.showNearestStore2 = showNearestStore2();
              
            }
          }


        });
      }.bind(undefined, count), count * 450);
    }  // end for
  }  // End Init2()


//  var showNearestStores = function() {
    // Get user's current location
//    if ("geolocation" in navigator) {
      /* geolocation is available */
//      navigator.geolocation.getCurrentPosition(function(position) {
       
//        setTimeout(function() {
//          argoGlobals.map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
//          argoGlobals.map.setZoom(6);
//        }, 500);
//      });
//    } else {
      /* geolocation IS NOT available */
//      $('.show-nearest-stores').slideUp();
//      alert("Couldn't find location.");
//    }
//  };
  // showNearestStores();


    var searchLocations = function() {
      // $('#search-locations-input').focus(function() {
      //   $( '#search-locations-input' ).scrollHere(parseInt($( '#top-nav-new' ).css('height')));
      // });


      var hideAllMarkers = function(all) {
        var bounds = new google.maps.LatLngBounds();
        for (var count = 0; count < all.length; count++) {
          all[count].m.setVisible(false);
          bounds.extend(all[count].m.getPosition());
        }
        setTimeout(function() {
          argoGlobals.map.fitBounds(bounds);
        }, 500);
      };

      var showAllMarkers = function(all) {
        var bounds = new google.maps.LatLngBounds();
        for (var count = 0; count < all.length; count++) {
          all[count].m.setVisible(true);
          bounds.extend(all[count].m.getPosition());
        }
        setTimeout(function() {
          argoGlobals.map.fitBounds(bounds);
        }, 500);
      };

      $('#search-locations-input').keyup(function() {
        var searchTerm = $(this).val().toLowerCase(),
            all = argoGlobals.markers,
            bounds = new google.maps.LatLngBounds();

        if (searchTerm.length > 2) {
          codeAddress(searchTerm,all,bounds,'a');//call the function
        }
        else if (searchTerm.length > 2) {
          // If search input is cleared, show all locations.
          showAllMarkers(all);
        }
      });

    };  // End searchLocations()
  searchLocations();

  var codeAddress = function(address, all, bounds, searched) {
    var distanceC = {} ,
        autocomplete = argoGlobals.autocomplete;
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
      var bounds = new google.maps.LatLngBounds();
      var places = autocomplete.getPlace();
      if (places.length == 0) {
        return;
      }
      else {
        codeAddress(places,all,bounds,'b');
      }
    });

    if(searched == 'a') {  // Called from #search-locations-input keyup.
      geocoder.geocode( {address:address}, function(results, status)
                       {
        if (status == google.maps.GeocoderStatus.OK)
        {
        
          var serPos = results[0].geometry.location;
          argoGlobals.userLocation = serPos;
          for (var count = 0; count < all.length; count++) {
            if(all[count].m != null){
              var address = all[count].a;
              var marker = all[count].m;
              distanceC[count] = google.maps.geometry.spherical.computeDistanceBetween(marker.getPosition(),serPos);

            }
          }
          var sortable = [];
          for (var dist in distanceC) {
            sortable.push([dist, distanceC[dist]])
            sortable.sort(function(a, b) {return a[1] - b[1]})
          }
          var resultStores = sortable;
          showNearestStores(resultStores);
          bounds.extend(all[resultStores[0][0]].m.getPosition());
          bounds.extend(all[resultStores[1][0]].m.getPosition());
          bounds.extend(all[resultStores[2][0]].m.getPosition());
          if (argoGlobals.map.zoom > 15) argoGlobals.map.setZoom(15);
          argoGlobals.map.fitBounds(bounds);
        } else {
          
        }
      });
    }
    else {
      if(searched === 'b') { // Called from place_changed
        var serPos = address.geometry.location;
        argoGlobals.userLocation = serPos;
        for (var count = 0; count < all.length; count++) {
          if(all[count].m != null){
            var address = all[count].a;
            var marker = all[count].m;
            distanceC[count] = google.maps.geometry.spherical.computeDistanceBetween(marker.getPosition(),serPos);
          }
        }
        var sortable = [];
        for (var dist in distanceC) {
          sortable.push([dist, distanceC[dist]])
          sortable.sort(function(a, b) {return a[1] - b[1]})
        }
        var resultStores = sortable;
        showNearestStores(resultStores);
        bounds.extend(all[resultStores[0][0]].m.getPosition());
        bounds.extend(all[resultStores[1][0]].m.getPosition());
        bounds.extend(all[resultStores[2][0]].m.getPosition());
        if (argoGlobals.map.zoom > 15) argoGlobals.map.setZoom(15);
        argoGlobals.map.fitBounds(bounds);
      }
      else {
       
        var serPos = address;
       
        for (var count = 0; count < all.length; count++) {
          if(all[count].m != null){
            var address = all[count].a;
            var marker = all[count].m;
            distanceC[count] = google.maps.geometry.spherical.computeDistanceBetween(marker.getPosition(),serPos);
          }
        }
        var sortable = [];
        for (var dist in distanceC) {
          sortable.push([dist, distanceC[dist]])
          sortable.sort(function(a, b) {return a[1] - b[1]})
        }
        var resultStores = sortable;
        showNearestStores(resultStores);
        bounds.extend(all[resultStores[0][0]].m.getPosition());
        bounds.extend(all[resultStores[1][0]].m.getPosition());
        bounds.extend(all[resultStores[2][0]].m.getPosition());
        if (argoGlobals.map.zoom > 15) argoGlobals.map.setZoom(15);
        argoGlobals.map.fitBounds(bounds);
      }

    }
  }


  //$('.show-nearest-stores').click(showNearestStores);
  $('.show-nearest-stores').click(showNearestStore2);

  var showNearestStores = function(sorted){
   
          var myNode = document.getElementById("nearest");
            while (myNode.firstChild) {
                myNode.removeChild(myNode.firstChild);
            }
    for(var i=0; i<3;i++){
          var j = parseInt(sorted[i][0]);
   
          var place = argoGlobals.storePlace[j];
          var div = document.createElement('div');
              div.className = 'near col-sm-10';
        var address= '';
            if ('formatted_address' in place) {
              address = place.formatted_address;
            }
      	  var str = '';
      try {
          if ('address_components' in place) {
            str = '<p class="text-left">';
            if (place.address_components.length >= 2)
              if ('long_name' in place.address_components[2])
                str += place.address_components[2].long_name + ', ';
            if (place.address_components.length >= 3)
              if ('long_name' in place.address_components[3])
                str += place.address_components[3].long_name + ', ';
            if (place.address_components.length >= 4)
              if ('long_name' in place.address_components[4])
                str += place.address_components[4].long_name + ', ';
            if (place.address_components.length >= 5)
              if ('long_name' in place.address_components[5])
                str += place.address_components[5].long_name;
            str += '</p>'
            // console.info(place, str, place.address_components);
          }
      }
      catch(e) {
        console.warn('Something went wrong while finding addresses for searched locations.', e);
        }
          var content;
          content = '<h3 class="text-left">' + place.name + '</h3>';
            if (place.address_components.length >= 1)
              if ('long_name' in place.address_components[1])
                content += '<p class="text-left">' + place.address_components[1].long_name + '</p>';
	      // content += '<p class="text-left">' + place.adr_address + '</p>';
          content += str;
           if ('international_phone_number' in place) content += '<p class="text-left phone">' + place.international_phone_number + '</p>';

          if ('opening_hours' in place) {
            content += '<div class="links clearfix"><a class="btn btn-primary pull-left see-hours-sidebar">SEE HOURS</a>';
          }
          content += '<a href="http://maps.google.com/maps?saddr=' + argoGlobals.userLocation + '&daddr=' + place.geometry.location + '" target="_blank" class="btn btn-primary pull-right">GET DIRECTIONS</a></div>';
        content += '<div class="sidebar-hours clearfix" style="display: none;">' + getFormattedHours(place) + '</div>';


          div.innerHTML = content;
          document.getElementById('nearest').appendChild(div);

       }
  }

  var showNearestStore2 = function() {
 
    // Show nearest store
    // Zoom map to show nearest store

    // print all distances
    var cl,  // Current Location
      dl,    // Destination Location
      service = new google.maps.DistanceMatrixService(),
      minDistance = {},
      lastLatLng,
      pairs = [];
    minDistance.distance = Infinity;

    var showNearest = function() {
   
      // var nearest = Infinity;
      // for (var count = 0; count < minDistance.length; count++) {
      //   if (minDistance[count].distance < nearest) nearest = minDistance[count];
      // }

      argoGlobals.map.setCenter(minDistance.pos);
      argoGlobals.map.setZoom(15);
    }
    var distanceCount = 0;

    function callback(response, status) {
      // See Parsing the Results for
      // the basics of a callback function.

      if (status == google.maps.DistanceMatrixStatus.OK) {
        var origins = response.originAddresses;
        var destinations = response.destinationAddresses;

        

        for (var i = 0; i < origins.length; i++) {
          var results = response.rows[i].elements;
          for (var j = 0; j < results.length; j++) {
            var element = results[j];
            console.info(element.distance !== undefined, element, typeof element);
            if (element.distance !== undefined) {
             
              pairs.push({ll: destinations, d: element.distance.value});
   
              if (element.distance.value < minDistance.distance) {
                console.info(element.distance.value);
                // minDistance = element.distance.value;
                // minDistance = {distance: element.distance.value, pos: destinations[0]};
                minDistance = {distance: element.distance.value, pos: argoGlobals.markers[distanceCount].m.position};
              }
            }
            //var distance = element.distance.text;
          }
        }
        distanceCount++;
        if (distanceCount === argoGlobals.markers.length) showNearest();
      }
    }

    if ("geolocation" in navigator) {
      var geoSuccess = function(position){
        var all = argoGlobals.markers,
            bounds = new google.maps.LatLngBounds(),
            cl = new google.maps.LatLng(parseFloat(position.coords.latitude), parseFloat(position.coords.longitude));
        argoGlobals.userLocation = cl;
        codeAddress(cl,all,bounds,false);
      };
      var geoError = function(position) {
        console.error('Error occurred while finding location.');
      };
      var geoOptions = {
        timeout: 10 * 1000,
        maximumAge: 5 * 60 * 1000
      }
      navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
    }
    else {
      /* geolocation IS NOT available */
      $('.show-nearest-stores').slideUp();
      alert("Couldn't find location. Your browser doesn't support geolocation.");
    }

    return {
      cl: cl,
      dl: dl,
      lastLatLng: lastLatLng,
      pairs: pairs
    }

  };  // End showNearestStore2()


  (function toggleSidebarHours() {
    $('.locations-cont').on('click', '.see-hours-sidebar', function() {
      var $hours = $(this).parent().parent().find('.sidebar-hours');
      if ($hours.css('display') === 'none') {
        $('.sidebar-hours').slideUp();
        $hours.slideDown();
      }
      else {
        $hours.slideUp();
      }
    });
  })();


  (function viewFullCafeDirectory() {
    if (!$('.show-full-directory').length) return;
    $('.show-full-directory').click(function(e) {
   
      e.preventDefault();
      $($(this).attr('href')).scrollHere();
    });
  })();

};  // End _AT.locations()
