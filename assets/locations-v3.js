// locations-v3.js

/* Behaviour for Locations Page
   http://argotea-2.myshopify.com/pages/locations */



var _AT = _AT || {};

var cafeGlobals = {
  map: null,
  infowindow: null,     // Instance of InfoWindow.
  userLocation: {},     // User's location found with Geolocation API.
  allCafes: {},         /* Data about all cafes. Holds static (name, address, etc.) as well as
                           dynamic data (LatLng object, distance from user's location, etc.). */
  autocomplete: null
};

cafeGlobals.allCafes = [
  // {
  //   title: '',
  //   coords: '',
  //   address: {
  //     street: '',
  //     city: 'Chicago',
  //     state: 'IL',
  //     country: 'USA',
  //     zip: '606'
  //   },
  //   hours: '',
  //   phone: '',
  //   google: {}
  // },
  {
    title: 'Broadway',
    coords: '41.938560, -87.644052',
    address: {
      street: '3135 N Broadway St',
      city: 'Chicago',
      state: 'IL',
      country: 'USA',
      zip: '60657'
    },
    hours: 'MON-FRI 6a-11p<br />SAT-SUN 7a-10p',
    phone: '(773) 248-3061',
    google: {}
  },
  //----------------------
  {
    title: 'Connors Park',
    coords: '41.898574, -87.626705',
    address: {
      street: '871 N Wabash Ave',
      city: 'Chicago',
      state: 'IL',
      country: 'USA',
      zip: '60611'
    },
    hours: 'MON-SUN 8a-9p',
    phone: '(773) 649-9644',
    google: {}
  },
  //----------------------
  {
    title: 'Franklin',
    coords: '41.881537, -87.634725',
    address: {
      street: '1 S Franklin St',
      city: 'Chicago',
      state: 'IL',
      country: 'USA',
      zip: '60606'
    },
    hours: 'MON-FRI 6a-8p<br />SAT - SUN Closed',
    phone: '(312) 267-0711',
    google: {}
  },
  //----------------------
  {
    title: 'Marquette',
    coords: '41.879800, -87.629439',
    address: {
      street: '140 S Dearborn St',
      city: 'Chicago',
      state: 'IL',
      country: 'USA',
      zip: '60603'
    },
    hours: 'MON-FRI 6a-7:30p<br />SAT 11a-5p<br />SUN Closed',
    phone: '(773) 663-4471',
    google: {}
  },
  //----------------------
  {
    title: 'Merchandise Mart',
    coords: '41.888590, -87.635233',
    address: {
      street: '222 W Merchandise Mart Plaza',
      city: 'Chicago',
      state: 'IL',
      country: 'USA',
      zip: '60654'
    },
    hours: 'MON-FRI 6:30a-6p<br />SAT - SUN Closed',
    phone: '(312) 324-3785',
    google: {}
  },
  //----------------------
  {
    title: '',
    coords: '41.884702, -87.62849289999997',
    address: {
      street: '16 W Randolph St',
      city: 'Chicago',
      state: 'IL',
      country: 'USA',
      zip: '60601'
    },
    hours: 'MON-FRI 6:30a-6p<br />SAT - SUN Closed',
    phone: '(312) 324-3785',
    google: {}
  },
  //----------------------
];


jQuery(function($) {
  cafeGlobals.map = cafes.initMap();
  cafes.addAllMarkers();
  cafes.showNearestStores();
  cafeGlobals.infowindow = cafes.addInfoWindow();
  cafeGlobals.autocomplete = cafes.initAutoComplete();
});

var cafes = (function() {
  var privateData = [];
  var map;
  function computeAllDistances(userLocation) {
    var position;
    for (var count = 0; count < cafeGlobals.allCafes.length; count++) {
      cafeGlobals.allCafes[count].google.distance = google.maps.geometry.spherical.computeDistanceBetween(cafeGlobals.allCafes[count].google.position, userLocation);
      
    }
  }

  function listenMarkerClicks(marker, cafe) {
    var cafe = findCafeByTitle(marker.argoCafe);
    // console.info(marker.argoCafe, cafe);
    google.maps.event.addListener(marker, 'click', function() {
      content = '<h3>' + cafe.title + '</h3>';
      content += '';
      content += '<p>' + cafe.hours + '</p>';
      content += '<p>' + cafe.address.street + '<br />';
      content += cafe.address.city + ', ';
      content += cafe.address.state + ', ';
      content += cafe.address.country + ', ';
      content += cafe.address.zip + '</p>';
      content += '<em>' + cafe.phone + '</em>';
      content += '<div class=""><a href="http://maps.google.com/maps?saddr=' + cafeGlobals.userLocation + '&daddr=' + marker.getPosition() + '" target="_blank" class="btn btn-xs btn-purple">GET DIRECTIONS</a></div>';
      cafeGlobals.infowindow.setContent(content);
      cafeGlobals.infowindow.setPosition(marker.getPosition());
      cafeGlobals.infowindow.open(cafeGlobals.map, this);
    });
  }

  function findCafeByTitle(title) {
    var ac = cafeGlobals.allCafes;
    for (var count = 0; count < ac.length; count++) {
      if (title === ac[count].title) {
        console.info('found', ac[count].title);
        return ac[count];
      }
    }
  }


  return {  // Public methods and properties. ------------------------------------------------------
    initMap: function() {
      var mapOptions = {
        center: { lat: 41.872458, lng: -87.676907 }, // Center at Chicago. Overriden later.
        zoom: 12,
        navigationControl: false,
        mapTypeControl: false,
        scaleControl: false,
        // draggable: false,
        scrollwheel: false
      };
      map = new google.maps.Map(document.getElementById('locationsMap'), mapOptions);
      return map;
    },


    addAllMarkers: function() {
      var ac = cafeGlobals.allCafes;
      var bounds = new google.maps.LatLngBounds();
      var c = 0;
      for (var count = 0; count < ac.length; count++) {
        c = c + 1;
        
        setTimeout(function(count) {
          var latlng = ac[count].coords.replace(' ', '');
          latlng = latlng.split(',');
          // console.warn(latlng);
          var lat = latlng[0];
          var lng = latlng[1];
          var gLatLng = new google.maps.LatLng(lat, lng);
          cafeGlobals.allCafes[count].google.position = gLatLng;
          console.info(count, lat, '-', lng, gLatLng);
          var marker = new google.maps.Marker({
            map: cafeGlobals.map,
            position: gLatLng,
            icon: 'https://cdn.shopify.com/s/files/1/0909/1186/t/6/assets/marker.png?l'
          });
          marker.argoCafe = ac[count].title;
          cafeGlobals.allCafes[count].google.marker = marker;
          listenMarkerClicks(marker);
          bounds.extend(gLatLng);
          if (c === Object.keys(cafeGlobals.allCafes).length)
            cafeGlobals.map.fitBounds(bounds);
        }.bind(undefined, count), c * 300);
      }
    },


    showNearestStores: function() {
      if ("geolocation" in navigator) {
        var geoSuccess = function(position){
          var all = cafeGlobals.allCafes,
            cl = new google.maps.LatLng(parseFloat(position.coords.latitude), parseFloat(position.coords.longitude));
          cafeGlobals.userLocation = cl;
          console.info(cafeGlobals.userLocation, cl, cafeGlobals.userLocation.lat(), cafeGlobals.userLocation.lng());
          computeAllDistances(cl);
          // Sort cafes by distance.
          // Fit bounds on 3 nearest stores.
        };
        var geoError = function(position) {
          console.error('Error occurred while finding location.');
        };
        var geoOptions = {
          timeout: 10 * 1000,
          maximumAge: 5 * 60 * 1000
        }
        navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
      }
      else {
        /* geolocation IS NOT available */
        $('.show-nearest-stores').slideUp();
        alert("Couldn't find location. Your browser doesn't support geolocation.");
      }
    },


    sortArray: function(inputArray) {
      inputArray.sort(function(a, b) {
        return a - b;
      });
    
      return inputArray;
    },


    sortObject: function(items, sortKey) {
      items.sort(function (a, b) {
        if (a[sortKey] > b[sortKey]) {
          return 1;
        }
        if (a[sortKey] < b[sortKey]) {
          return -1;
        }
        // a must be equal to b
        return 0;
      });
      return items;
    },


    addInfoWindow: function() {
      var infowindow = new google.maps.InfoWindow();
      return infowindow;
    },


    initAutoComplete: function() {
      var input = document.getElementById('search-locations-input');
      var autocomplete = new google.maps.places.Autocomplete(input);
      return autocomplete;
    }
    
    
    

  };
}());


(function viewFullCafeDirectory() {
    if (!$('.show-full-directory').length) return;
    $('.show-full-directory').click(function(e) {
      
      e.preventDefault();
      $($(this).attr('href')).scrollHere();
    });
})();




$("#locs").on("click", "#location1", function() {
        google.maps.event.trigger(gmarkers[0], "click");
        var laLatLng = new google.maps.LatLng(41.884702, -87.62849289999997);
        map.panTo(laLatLng);
    });

