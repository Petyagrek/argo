"use strict";



/* Behaviour for Locations Page
   http://argotea-2.myshopify.com/pages/locations */



var _AT = _AT || {};

var _AtGlobals = {
  map: null,
  markers: [],          // Init empty array so that markers can be pushed to it.
  iws: [],              // For all InfoWindows.
  infoWindow: null,  // There's only one InfoWindow
  doneReq: false,
  userLocation: {},
  storePlace: {},
  allCafes: {},
  sortedByDistance: []
};



jQuery(function($) {


  (function() {
    if(!$('body').hasClass('page-locations-v2')) return;

    var map;
    var bounds;
    var service;
    var geocoder = new google.maps.Geocoder();
    _AtGlobals.infoWindow = new google.maps.InfoWindow();


    function initialize() {
      var mapOptions = {
        center: { lat: 41.872458, lng: -87.676907 }, // Center at Chicago. Overriden later.
        zoom: 12,
        navigationControl: false,
        mapTypeControl: false,
        scaleControl: false,
        // draggable: false,
        scrollwheel: false
      };
      map = new google.maps.Map(document.getElementById('cafeLocationsMap'), mapOptions);
      _AtGlobals.map = map;
      bounds = new google.maps.LatLngBounds();
      addAllMarkers();
    }
    google.maps.event.addDomListener(window, 'load', initialize);



    // Place IDs can be found at:
    // https://developers.google.com/maps/documentation/javascript/examples/places-placeid-finder
    _AtGlobals.locationIds = [
      'ChIJETsFTaUsDogRwB5N0aWJ-kE', // State & Randolph ✔
      'ChIJP_NOo7wsDogR9TcBHPMn2GM', // Dearborn & Adams ✔
      'ChIJ6dpUbjkpDogRSuTvQmsXywA', // 5758 S Maryland Ave ✔
      'ChIJ43ZVwabTD4gRfnQpDg-taIo', // 3135 North Broadway ✔
      'ChIJ-72Y31TTD4gRk8E39cYxIRA', // Northwestern Memorial Hospital ✔
      'ChIJV0LZUbYsDogRU8BFdH1OjPM', // 222 W Merchandise Mart Plaza ✔
      'ChIJpafdA1PTD4gRTbdqGGo3j7I', // 819 N Rush St, Chicago, IL, United States ✔
      'ChIJqQyWfSa0D4gRN_IZ45o_HCE', // Terminal 2 ✔
      'ChIJRSn-ONe1D4gR9yZD1uq6t4k', // Terminal 3, O'Hare International Airport, Chicago, IL, United States ✔
      'ChIJ6QM3Tim0D4gROoQMax1M5oE', // Terminal 3
      'ChIJA98_PLksDogRa_L2mJG8kAA', // Franklin & Madison ✔
      'ChIJu_tp4r4sDogRLarHLYUcYUs', // The Willis Tower - Wacker Drive ✔
      'ChIJ-QQV-aNZwokRfC_bYyZuq6E', // 949 Broadway ✔
      'ChIJcb-IBQ9ZwokRrMAlKCsa4_U', // Nyuu Medical Center: Bakshy Aric MD ✔
      'ChIJ8-NotfdYwokRnXBumM5iYW8', // Columbus Circle ✔
      'ChIJRygdwplZwokRhLLHDf10kCA', // Union Square ✔
      'ChIJj2q4SKVZwokR4ifdE5XHjyQ', // Chelsea ✔
      'ChIJb6CL_qssDogRCC9pTVDZWBk', // Tribune Tower ✔
      'ChIJ2d67RVLTD4gRtkRmcRqUB_M', // Connors Park ✔
      'ChIJS_rmxJavD4gRw6AAriTq_AI', // Woodfield Mall ✔
      'ChIJu7jHY5BZwokRMXOlqm74HQM', // 239 Greene St ✔
      'ChIJ_zScxzxmXj4R3qCFAp08HmE', // Al Wahdah ✔
      'ChIJASzND9cS04kRjIJ-qAVQvg8', // Buffalo State ✔
      'ChIJjzmdDB-jGYgR-jLs4LNnA-o', // Grand Valley State University
      'ChIJWSc92xgKzz8RM1NpafGVQe0', // The Gate Mall ✔
      'ChIJVWuaL2Sczz8RbwhGhHVXAMo', // Tavern 2 ✔
      'ChIJAVkD4S_99IgRSLCSGO-jF0E', // Atlanta Airport 
      'ChIJ3cRJt0iGs4kRRliLzMIIUUA', // University Of Virginia
      // 'ChIJc6UJTuAh9ocRWPZM68X07Cw', // UHG, 11000 Optum Circle
      'ChIJ65GMS-Ah9ocRMbFEZ5Um2eo', // UnitedHealth Group Optum Headquarters, 11000 Optum Circle
      'ChIJjdor14Tmt4kRdwPWxeHN7G4', // Fort Meade, MD
      'ChIJ8QxqrKw1R4YRGtaELHbGOpc', // Exxon Mobil, TX
      'ChIJRxq6yTad44kRqpAmDsj5H4Y', // Bentley University, Waltham
      'ChIJDwOafn1FXj4RGAdtSOLgiek', // Yas Mall
      'ChIJNcvFFTsL3okRwjfsDm0SUVE', // University of Albany
      'ChIJ3RFkxAkpU4gRCZzO6Zydjk4', // Elon University
      'ChIJdTmri3qJsIkRQN3gvl6vw70', // College of William and Mary
      'ChIJC-tmgWhCXz4RL3vgur4gUkc', // City Walk
      'ChIJc62H4EuLWYgRuh1Xif87e8Y', // UNC
      'ChIJs9U8YuPJt4kRzrhvQqTY8i4', // American University
      'ChIJq5JzPW_cRT4RtqbA-pWTHQI', // Gulf Mall Doha
      'ChIJlWIAxhgKzz8RHC62j-H-U1g', // Gate Mall, Kuwait
      'ChIJvRAbEIwELz4R2U1lO8VrtVQ', // Riyadh Avenue Mall
      'ChIJpdZYwbDIRT4R0zDtL-03UYU', // Hamad Airport
      'ChIJHfeKyWTcRT4R_Jc167s7Xq0' // Landmark Mall
      // 'ChIJjzmdDB-jGYgR-jLs4LNnA-o'  // GVSU
    ];
    var locationIds = _AtGlobals.locationIds;



    var getFormattedHours = function(place) {
      if ('opening_hours' in place) {
        var hours = '';
        var content = '<pre>';
        for (var c = 0; c < place.opening_hours.weekday_text.length; c++) {
          hours = place.opening_hours.weekday_text[c];
          hours = hours.replace('Sunday:', 'Sunday:   ');
          hours = hours.replace('Monday:', 'Monday:   ');
          hours = hours.replace('Tuesday:', 'Tuesday:  ');
          hours = hours.replace('Wednesday:', 'Wednesday:');
          hours = hours.replace('Thursday:', 'Thursday :');
          hours = hours.replace('Friday:', 'Friday:   ');
          hours = hours.replace('Saturday:', 'Saturday: ');
          content += hours + '<br />';
        }
        content += '</pre>';
      }
      else {
        console.warn('opening_hours not found for', place.name);
      }
      return content;
    }


    var storageAvailable = function(type) {
      try {
        var storage = window[type],
          x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
      }
      catch(e) {
        return false;
      }
    };


    var populateLocalStorage = function(id, cafeInfo) {
      if (localStorage.getItem(id) == null) {
        localStorage.setItem('id--' + id, 'available');
        localStorage.setItem('lat--' + id, cafeInfo.lat);
        localStorage.setItem('lng--' + id, cafeInfo.lng);
        localStorage.setItem('name--' + id, cafeInfo.name);
        if ('stringifiedHours' in cafeInfo) localStorage.setItem('formattedHours--' + id, cafeInfo.stringifiedHours);
        localStorage.setItem('phone--' + id, cafeInfo.phone);
        localStorage.setItem('address--' + id, cafeInfo.address);
      }
    };


    var constructCafeFromLocalStorage = function(id) {
      var place = {};
      place.geometry = {};
      if (localStorage.getItem('lat--' + id) !== null && localStorage.getItem('lng--' + id) !== null) {
        place.geometry.location = new google.maps.LatLng(localStorage.getItem('lat--' + id), localStorage.getItem('lng--' + id));
      }
      place.name = localStorage.getItem('name--' + id);
      if (localStorage.getItem('formattedHours--' + id) != null) {
        place.opening_hours = JSON.parse(localStorage.getItem('formattedHours--' + id));
      }
      place.international_phone_number = localStorage.getItem('phone--' + id);
      place.adr_address = localStorage.getItem('address--' + id);
      // var formatted_address = localStorage.getItem('address--' + id);
      // formatted_address = $(formatted_address).text()
      // console.info('formatted_address', formatted_address);
      // place.unformatted_address = formatted_address;
      if (id in _AtGlobals.allCafes) {
        _AtGlobals.allCafes[id].place = place ;
      }
      else {
        _AtGlobals.allCafes[id] = { place: place };
      }
      // console.info('place from local storage:', place);
      return place;
    };


    var constructAllCafesFromLocalStorage = function() {
      for (var key in _AtGlobals.allCafes) {
        constructCafeFromLocalStorage(key);
      }
    };


    var constructInfoWindowContents = function(place, marker) {
      var content = '<h3>' + place.name + '</h3>';
      content += getFormattedHours(place);
      content += '<p>' + place.adr_address + '</p>';
      if ('international_phone_number' in place) content += '<em class="pull-left">' + place.international_phone_number + '</em>';
      content += '<div class="pull-right"><a href="http://maps.google.com/maps?saddr=' + _AtGlobals.userLocation + '&daddr=' + marker.getPosition() + '" target="_blank" class="btn btn-xs btn-purple">GET DIRECTIONS</a></div>'

      return content;
    }


    var processLocation = function(place, count, stat) {
      if(stat){
        var address= '';
        if ('formatted_address' in place) {
          address = place.formatted_address;
        }

        var marker = new google.maps.Marker({
          map: _AtGlobals.map,
          position: place.geometry.location,
          icon: 'https://cdn.shopify.com/s/files/1/0909/1186/t/6/assets/marker.png?l'
        });
        _AtGlobals.markers.push({a: address, m: marker});
        if (locationIds[count] in _AtGlobals.allCafes) {
          _AtGlobals.allCafes[locationIds[count]].marker = marker ;
        }
        else {
          _AtGlobals.allCafes[locationIds[count]] = { marker: marker };
        }

        // var infowindow = new google.maps.InfoWindow();
        var content;
        google.maps.event.addListener(marker, 'click', function() {
          content = constructInfoWindowContents(place, marker);
          _AtGlobals.infoWindow.setContent(content);
          _AtGlobals.infoWindow.open(_AtGlobals.map, this);
        });
      }
      else {
        _AtGlobals.markers.push({a: null, m: null});
      }
    };  // End processLocation()


    var showNearestCafe = function(selectedLocation) {
      _AtGlobals.sortedByDistance = [];
     
      for (var key in _AtGlobals.allCafes) {
        if (!'place' in _AtGlobals.allCafes[key]) {
          console.warn('place not found in showNearestCafe');
          continue;
        }
        var thePlace = _AtGlobals.allCafes[key].place;
        if (thePlace === undefined) {
          console.warn('place is undefined in showNearestCafe. Trying to construct cafes from localStorage.');
          constructAllCafesFromLocalStorage();
          continue;
        }
        if ('geometry' in thePlace !== true) { console.warn('geometry not found in place in showNearestCafe'); continue; }
    
        var distance = google.maps.geometry.spherical.computeDistanceBetween(selectedLocation, _AtGlobals.allCafes[key].place.geometry.location);
        _AtGlobals.sortedByDistance.push({id: key, distance: distance});
        // _AtGlobals.allCafes[key].distanceFromSelectedPlace = distance;
       
      }


      setTimeout(function() {
        _AtGlobals.sortedByDistance.sort(function (a, b) {
          if (a.distance > b.distance) {
            return 1;
          }
          if (a.distance < b.distance) {
            return -1;
          }
          // a must be equal to b
          return 0;
        });
      
        var bounds = new google.maps.LatLngBounds();
        bounds.extend(_AtGlobals.allCafes[_AtGlobals.sortedByDistance[1].id].place.geometry.location);
        bounds.extend(_AtGlobals.allCafes[_AtGlobals.sortedByDistance[2].id].place.geometry.location);
        bounds.extend(_AtGlobals.allCafes[_AtGlobals.sortedByDistance[3].id].place.geometry.location);
        _AtGlobals.map.fitBounds(bounds);
      }, 1000);

      return _AtGlobals.sortedByDistance;
    };


    var addAllMarkers = function() {
      var stat = true;
      var $foundCafeCount = $('.locations-status strong');
      service = new google.maps.places.PlacesService(_AtGlobals.map);
      $('.locations-status span').text(locationIds.length);
      for (var count = 0; count < locationIds.length; count++) {
        if (localStorage.getItem('id--' + locationIds[count]) !== 'available') {
        // if (true) {
          setTimeout(function(count) {
            var request = { placeId: locationIds[count] };
            
            service.getDetails(request, function (place, status) {
              if (status == google.maps.places.PlacesServiceStatus.OK) {
                // console.info('place', place);
                $foundCafeCount.text(count + 1);
                if (count === locationIds.length - 1) {
                  $('.locations-status').fadeOut();
                  $('#search-locations-input-v2').removeAttr('disabled');
                }

                if (storageAvailable('localStorage')) { // Store place data locally in browser.
                  var localStorageItems = {
                    lat: place.geometry.location.lat(),
                    lng: place.geometry.location.lng(),
                    name: place.name,
                    phone: place.international_phone_number,
                    address: place.adr_address
                  }
                  if ('opening_hours' in place) {
                   
                    localStorageItems.stringifiedHours = JSON.stringify(place.opening_hours);
                  }
                  populateLocalStorage(locationIds[count], localStorageItems);
                }

            
                _AtGlobals.storePlace[count] = place;
                processLocation(place, count, stat);
              } else {
                _AtGlobals.failReq = _AtGlobals.failReq + 1;
                stat = false;
                _AtGlobals.storePlace[count] = null;
                console.error(count, locationIds[count], status, place);
                processLocation(place, count, stat);
              }
              
            
              if(count == locationIds.length -1) {
                _AtGlobals.doneReq = true;
                if (_AtGlobals.doneReq) {
                  // $('.locations-cont .loading').slideUp();
                  // After all markers are added, show nearest stores.
                  // showNearestCafe(place.geometry.location);
                  showNearestCafe(_AtGlobals.userLocation);
                  
                }
              }
            });
          }.bind(undefined, count), count * 450);
        }
        else {
          var place = constructCafeFromLocalStorage(locationIds[count]);
          $foundCafeCount.text(count + 1);
          if (count === locationIds.length - 1) {
            // showNearestCafe(place.geometry.location);
            showNearestCafe(_AtGlobals.userLocation);
            $('.locations-status').fadeOut();
            $('#search-locations-input-v2').removeAttr('disabled');
          }
          processLocation(place, count, stat);
        }
      }  // end for
      // showNearestCafe(_AtGlobals.userLocation);
    } // End addAllMarkers()





    if ("geolocation" in navigator) {
      var geoSuccess = function(position){
        var all = _AtGlobals.markers,
          bounds = new google.maps.LatLngBounds(),
          cl = new google.maps.LatLng(parseFloat(position.coords.latitude), parseFloat(position.coords.longitude));
        _AtGlobals.userLocation = cl;
        showNearestCafe(_AtGlobals.userLocation);
      };
      var geoError = function(position) {
        console.error('Error occurred while finding location.');
      };
      var geoOptions = {
        timeout: 10 * 1000,
        maximumAge: 5 * 60 * 1000
      }
      navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
    }
    else {
      /* geolocation IS NOT available */
      alert("Couldn't find location. Your browser doesn't support geolocation.");
    }


    (function toggleSidebarHours() {
      $('.locations-cont').on('click', '.see-hours-sidebar', function() {
        var $hours = $(this).parent().parent().find('.sidebar-hours');
        if ($hours.css('display') === 'none') {
          $('.sidebar-hours').slideUp();
          $hours.slideDown();
        }
        else {
          $hours.slideUp();
        }
      });
    })();


    (function viewFullCafeDirectory() {
      if (!$('.show-full-directory').length) return;
      $('.show-full-directory').click(function(e) {
       
        e.preventDefault();
        $($(this).attr('href')).scrollHere(100);
      });
    })();


    (function inputSearch() {
      if (!$('#search-locations-input-v2').length) return;
      var $cafeResults = $('.cafe-results'),
        allCafes = _AtGlobals.allCafes,
        $searchInput = $('#search-locations-input-v2');
      $('#search-locations-input-v2').keyup(function(e) {
        if ($(this).val().length > 3) {
          $cafeResults.html('');
          

          // Find matching stores
          var searchTerm = $(this).val();
          searchTerm.toLowerCase();
          var re = new RegExp('(' + searchTerm + ')', 'i');
          var resultLocations = [];
          var matchFound = false;
          for (var key in allCafes) {
           
            var place = allCafes[key].place;
            var addressLC = place.adr_address.toLowerCase();
           
            $searchInput.tooltip('hide');
            if (addressLC.match(re) !== null) {
           
              var li = ['<div class="search-result-location"><h4 class="pull-left">' + place.name + '</h4>',
                '<a class="btn pull-right btn-purple btn-xs locate-btn" href="#" data-cafe-id="' + key + '"><i class="fa fa-map-marker"></i> &nbsp; LOCATE</a>',
                '<p class="clearfix">' + place.adr_address + '</p>',
                // '<p><a target="_blank" class="btn btn-primary btn-xs" href="http://maps.google.com/maps?saddr=' + _AtGlobals.userLocation + '&daddr=' + allCafes[key].marker.getPosition() + '">GET DIRECTIONS</a> &nbsp; ',
                '</div>'].join('');
              $cafeResults.append(li);
              $cafeResults.show();
              resultLocations.push(allCafes[key].marker.getPosition());
              matchFound = true;
            }
          }
          if (matchFound === false) {
            showNearestCafe(_AtGlobals.userLocation);
            $searchInput.tooltip('show');
          }
          bounds = new google.maps.LatLngBounds();
          for (var count = 0; count < resultLocations.length; ++count) {
            bounds.extend(resultLocations[count]);
          }
          _AtGlobals.map.fitBounds(bounds);
        }
        else {
          $cafeResults.html('');
          $cafeResults.hide();
          // showNearestCafe(_AtGlobals.userLocation);
          // $searchInput.tooltip('show');
        }
      });
    })();


    (function locateButtonInSearchResults() {
      $('.cafe-results').on('click', '.locate-btn', function(e) {
        e.preventDefault();
        var cafeID = $(this).data('cafe-id'),
          cafePosition = _AtGlobals.allCafes[cafeID].marker.getPosition(),
          marker = _AtGlobals.allCafes[cafeID].marker;
      
        _AtGlobals.map.setCenter(cafePosition);
        _AtGlobals.map.setZoom(14);
        _AtGlobals.infoWindow.close();
        var content = constructInfoWindowContents(_AtGlobals.allCafes[cafeID].place, marker);
        _AtGlobals.infoWindow.setContent(content);
        _AtGlobals.infoWindow.open(_AtGlobals.map, marker);
        // _AtGlobals.infoWindow.setPosition(cafePosition);
      });
    })();

  })();


});

