$(function() {
    
    
    
    
    
                        var $quickViewBtn = $(".button-quick"),
                            $quickView = $('#quickview'),
                            $quickViewOvrl = $('.ovrl', $quickView),
                            $quickViewCnt = $('.inner', $quickView);

                        //QuickView Btn
                        $quickViewBtn.click(function(e) {
                            e.preventDefault();
                            var $el = $(e.currentTarget),
                                productHandle = $el.data('handle'),
                                url = $el.data('url'),
                                productid = $el.data('productid'),
                                html = '';


                            $.getJSON('/products/' + productHandle + '.js', function(r) {
                                createQuickView(r, url); 
                                
                            });
                            
                            return;
                        });
                        
                


                    function createQuickView(r, url) {
                       
                        var productid = r.id,
                            description = r.description;

                        var html =
                            '<div class="row">' +
                            '<div class="col-md-5">' +
                            '<div id="quickViewCarousel">';
                        for (i in r.images) {
                            html +=
                                '<div class="item">' +
                                '<img src="' + r.images[i] + ' alt="image_' + i + '"/>' +
                                '</div>';
                        }
                        html += '</div>' +
                            '</div>' +
                            '<div class="col-md-7">' +
                            '<h2 class="title">' + r.title + '</h2>' +
                            '<div id="sprproductid"></div>' +
                            '<h3 class="price"> $ ' + (r.price_max / 100).toFixed(2) + '</h3>' +
                            '<form action="/cart/add" method="post" enctype="multipart/form-data" id="AddToCartForm" class="form-vertical">' +
                            '<div class="description">' +
                            '<div class="descriptiontext">' +
                             description +
                            '</div>' +
                                                            '<div class="col-md-4 mt hide">'+
                                                                '<label id="productSelect">'+r.options[0].name+'</label>'+
                                                                    '<select name="id" id="productSelect" style="width:100%;" class="product-single__variants">';

                                                                       for (v in r.variants){
                                                                           if (r.variants[v].available){
                                                                            html +='<option data-sku="'+r.variants[v].sku+'" value="'+r.variants[v].id+'">'+r.variants[v].title+' &nbsp; $'+(r.variants[v].price/100).toFixed(2)+'</option>';
                                                                           }
                                                                       }

                                       html +=                      '</select>'+
                                                            '</div>'+
                            '<div class="row">' +
                            '<div class="col-lg-12">' +
                            '<div class="quantity-wrap">' +
                            '<span><i class="fa fa-minus"></i></span>' +
                            '<input class="form-control input-sm" type="number" id="quantity" placeholder="Quantity" name="quantity" value="1" min="0">' +
                            '<span><i class="fa fa-plus"></i></span>' +
                            '</div>' +
                            '</div>' +
                            '<div class="col-md-12">' +
                            '<button type="submit" name="add" id="AddToCart" class="purchase-btn btn-solid btn-purple addtocart btn-primary">' +
                            '<span id="AddToCartText">ADD TO CART</span>' +
                            '</button>' +
                            '</div>' +

                            '<div class="col-md-12">' +
                            '<a href="' + r.url + '" class="full-prod animAll">View full product details</a>' +
                            '</div>' +
                            '</div>' + 


                            '</div>';
                        html += '</form>' +
                            '</div>' +
                            '</div>';





                        $quickViewCnt.html('');
                        $quickViewCnt.html(html);
                        $quickView.fadeIn();
                        setTimeout(function() {
                            //Product Review Badges -- re-initialize
                            var turgetRevews = '#spr_badge_' + productid;
                            var appendRevews = '#spr' + productid;
                            var htmlRevews = $(turgetRevews).html();
                            var htmlRev = htmlRevews;
                            $('#sprproductid').append(htmlRev);
                            //Product Review Badges -- re-initialize
                            //Quantity on Quick View
                            $('.quantity-wrap span').click(function(e) {
                                e.stopPropagation();
                                var val = parseInt($('#quantity').val());
                                if ($(this).find('i').hasClass('fa-minus')) { // Decrease quantity by 1.
                                    if (val === 1) return; // Quantity should be at least 1.
                                    $('#quantity').val(val - 1);
                                } else { // Increase quantity by 1. 
                                    $('#quantity').val(val + 1);
                                }
                            });
                            //Quantity on Quick View
                            //Owl
                              var imageSize = r.images;
                              if( imageSize.length > 1){
                                var nav = true;
                              } else {
                                var nav = false;  
                              }

                            $('#quickViewCarousel').owlCarousel({
                                items: 1,
                                loop: false,
                                margin: 10,
                                nav: nav,
                                navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
                                autoplay: false,
                                autoplayHoverPause: false
                            });
                            //Owl

                            $quickViewCnt.fadeIn();

                        }, 0);
                        
                    }   


           

    

    
      $('.button-quick').magnificPopup({
          items: {
              src: '#quickview',
              type: 'inline',
              removalDelay: 500,
              fixedBgPos: true,
              autoFocusLast: false, //If set to true last focused element before popup showup will be focused after popup 
          },
           callbacks: {
                beforeOpen: function() {
                   
                },
                open: function() {
                  
                },
                afterClose: function(item) {
                   var $quickViewCnt = $('.inner');
                   $quickViewCnt.html('');  
                }
           },
          closeBtnInside: true
        });
     
});